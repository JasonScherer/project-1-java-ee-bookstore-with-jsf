<%-- 
    Document   : add
    Created on : Feb 21, 2014, 9:08:47 PM
    Author     : schereja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="main.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add new book</title>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <h1>
                    JRS-BookStore</h1>
            </div>
            <div id="wrapper">
                <div id="content">
        <form method="POST" id="newBook" action="AdminAction">
            <table id="addBookTable">
                <td>
                <tr>Book Name: <input type="text" name="bookTitle" id="bookTitle" placeholder="1984"></tr>
                <tr>Book Description:<input type="text" name="bookDescription" id="bookDescription" placeholder="A story about big brother government."></tr>
                <tr>Book Price: <input type="text" name="bookPrice" id="bookPrice" placeholder="12.99"></tr>
                </td>
                <input type="submit" id="newBook" value="newBook" name="action" >
            </table>
            
        </form>
    </div>
                <div id="navigation">
                    <p>
                    <form method="POST" action="BookStore">
                        <input type="submit" id="BookStore" name="BookStore" value="BookStore">
                    </form>
                    <form method="POST" action="Admin">
                        <input type="submit" id="Admin" name="Admin" value="Admin">
                    </form>
                    </p>
                </div>
                <div id="extra">
                    <p>${bookAdded}</p>
                </div>
                <div id="footer">
                    <p>
                        <a href="mailto:${email}">Email Me @: ${email}</a></p>
                </div>
            </div>
    </body>
</html>
