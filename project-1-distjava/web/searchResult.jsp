<%-- 
    Document   : searchResult
    Created on : Feb 25, 2014, 11:06:34 PM
    Author     : schereja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="main.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Book Store</title>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <h1>
                    JRS-BookStore</h1>
            </div>
            <div id="wrapper">
                <div id="content">
                    <form id="book" method="GET" action="AdminAction">
                        <table border="1" id="bookTables">
                            <tr>
                                <td>Check</td>
                                <td>Book ID</td>
                                <td>Book Name</td>
                                <td>Book Price</td>
                            </tr>
                            
                                <tr>
                                    <td><input type="checkbox" name="selectedBook" value="${foundBook.bookId}"></td>
                                    <td id="bookId">${foundBook.bookId}</td>

                                    <td id="bookDesc">${foundBook.bookName}</td>

                                    <td id="bookPrice">${foundBook.bookPrice}</td>
                                </tr>
                                
                            

                        </table>
                        <input type="submit" value="edit" name="action" id="edit">
                        <input type="submit" value="add" name="action" id="add">
                        <input type="submit" value="delete" name="action" id="delete">
                    </form>
                    <form id="findBook" method="POST" action="AdminAction">
                        Please select field to search by:<br/>
                        <input type="radio" name="searchField" value="Book_Id">Book ID<br/>
                        <input type="radio" name="searchField" value="Book_Name">Book Name<br/>
                        <input type="radio" name="searchField" value="Book_Desc">Book Description<br/>

                        Please enter value:<input type="text" name="searchValue" id="searchValue">
                        <br/>
                        <input type="submit" name="action" value="findBook" id="findBook">
                    </form>
                </div>
                <div id="navigation">
                    <p>
                    <form method="POST" action="BookStore">
                        <input type="submit" id="BookStore" name="BookStore" value="BookStore">
                    </form>
                    <form method="POST" action="Admin">
                        <input type="submit" id="Admin" name="Admin" value="Admin">
                    </form>
                    </p>
                </div>
                <div id="extra">
                    <p></p>
                </div>
                <div id="footer">
                    <p>
                        <a href="mailto:${email}">Email Me @: ${email}</a></p>
                </div>
            </div>
    </body>
</html>