<%-- 
    Document   : edit
    Created on : Feb 21, 2014, 8:36:45 PM
    Author     : schereja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="main.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Book</title>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <h1>
                    JRS-BookStore</h1>
            </div>
            <div id="wrapper">
                <div id="content">
                    <form method="GET" id="editBook" action="AdminAction">
                        <table id="editBookTable">
                            <tr>
                                <td id="bookId" name="bookId">Book ID</td>

                                <td id="bookName" name="bookName">Book Name</td>
                                <td id="bookDesc" name="bookDesc">Book Description</td>

                                <td id="bookPrice">Book Price</td>
                            </tr><br />
                            <tr>
                                <td id="bookId" name="bookId">${book.bookId}</td>

                                <td id="bookName" name="bookName">${book.bookName}</td>
                                <td id="bookDesc" name="bookDesc">${book.bookDesc}</td>

                                <td id="bookPrice">${book.bookPrice}</td>
                            </tr><br />
                            <tr>
                                <td><input type="hidden" name="editBookId" id="editBookId" value="${book.bookId}">${book.bookId}</td>
                                <td><input type="text" name="editBookName" placeholder="${book.bookName}"></td>
                                <td><input type="text" name="editBookDesc" placeholder="${book.bookDesc}"></td>
                                <td><input type="text" name="editBookPrice" placeholder="${book.bookPrice}"></td>
                            
                            </tr>
                            <td><input type="submit" name="action" value="editBook" id="editBook"></td>
                        </table>

                    </form>

                </div>
                <div id="navigation">
                    <p>
                    <form method="POST" action="BookStore">
                        <input type="submit" id="BookStore" name="BookStore" value="BookStore">
                    </form>
                    <form method="POST" action="Admin">
                        <input type="submit" id="Admin" name="Admin" value="Admin">
                    </form>
                    </p>
                </div>
                <div id="extra">
                    <p>
                        ${bookEdited}
                    </p>
                </div>
                <div id="footer">
                    <p>
                        <a href="mailto:${email}">Email Me @: ${email}</a></p>
                </div>
            </div>
    </body>
</html>
