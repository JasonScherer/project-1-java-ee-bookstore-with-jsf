<%-- 
    Document   : orderSubmitted
    Created on : Feb 24, 2014, 9:28:59 PM
    Author     : schereja
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Order Submitted</title>
        <link rel="stylesheet" type="text/css" href="main.css">
    </head>
    <body>
        <div id="container">
            <div id="header">
                <h1>
                    JRS-BookStore</h1>
            </div>
            <div id="wrapper">
                <div id="content">
                    Thank you for your order:
                    <table>
                        <td>
                            ${fullAddress}
                        </td>

                        <c:forEach var="Book" items="${books}">

                            <tr>
                                <td id="bookId">${Book.bookId}</td>
                                <td id="bookName">${Book.bookName}</td>
                                <td id="bookPrice">${Book.bookPrice}</td>
                            </tr>
                        </c:forEach>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Tax: 6.5%</td>
                        </tr>  
                        <tr>
                            <td></td>
                            <td></td>
                            <td>${finalPrice}</td>
                        </tr>  

                    </table>
                </div>
            </div>
            <div id="navigation">

                <p>
                <form method="POST" action="BookStore">
                    <input type="submit" id="BookStore" name="BookStore" value="BookStore">
                </form>
                <form method="POST" action="Admin">
                    <input type="submit" id="Admin" name="Admin" value="Admin">
                </form>
                ﻿</p>
                </p>
            </div>
            <div id="extra">
                <p>
                 
                </p>
            </div>
            <div id="footer">
                <p>
                    <a href="mailto:${email}">Email Me @: ${email}</a></p>
            </div>
        </div>

    </body>
</html>
