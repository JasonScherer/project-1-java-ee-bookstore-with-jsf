<%-- 
    Document   : confimation
    Created on : Feb 23, 2014, 5:20:58 PM
    Author     : schereja
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page import="model.Book"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="false" %>   
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="main.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Confirmation Page</title>
    </head>
    <body>
        
        <div id="container">
            <div id="header">
                <h1>
                    JRS-BookStore</h1>
            </div>
            <div id="wrapper">
                <div id="content">
        <form id="confirmOrder" method="POST" action="Order">
            Please Confirm your order:<br />
            ${fullAddress}
            <table border ="1" id="order">
                <c:forEach var="Book" items="${orderedBooks}">
                    <form id="book" method="POST" action="Order">

                        <tr>
                            <td id="bookId">${Book.bookId}</td>
                            <td id="bookName">${Book.bookName}</td>
                            <td id="bookPrice">${Book.bookPrice}</td>

                        </tr>


                    </c:forEach>
                    <tr>
                        ${finalPrice}
                    </tr>  
            </table>

            Credit Card Number:<input type="text" id="cc" name="cc" value="cc">(unknown why but only cc value works)
            <input type="submit" id="submitOrder" value="Confirm Order" name="submitOrder">
        </form>

                </div>
                <div id="navigation">
                    <p>
                    <form method="POST" action="BookStore">
                        <input type="submit" id="BookStore" name="BookStore" value="BookStore">
                    </form>
                    <form method="POST" action="Admin">
                        <input type="submit" id="Admin" name="Admin" value="Admin">
                    </form>
                    </p>
                </div>
                <div id="extra">
                    <p>
                   </p>
                </div>
                <div id="footer">
                    <p>
                        <a href="mailto:${email}">Email Me @: ${email}</a></p>
                </div>
            </div>

    </body>
</html>
