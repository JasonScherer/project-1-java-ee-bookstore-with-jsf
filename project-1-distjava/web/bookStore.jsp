<%-- 
    Document   : storeFront
    Created on : Feb 18, 2014, 4:50:35 PM
    Author     : schereja
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page import="model.Book"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% int numOfItemsOrdered = 0;%>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="main.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>
            var numberOfItemsOrdered = 0;
            $(document).ready(function() {
                $("#orderContainer").hide();
            });
            function addToOrder(id, bookName, bookPrice) {
                $("#orderContainer").show();

                var newRow;
                newRow = '<tr><td>' + '<input type="hidden" name=book' + numberOfItemsOrdered + ' value=' + id + ' >' + bookName + '---$' + bookPrice + '</td></tr>';
                $("#order").append(newRow);
                numberOfItemsOrdered++;
                document.getElementById("numBooksOrdered").value = numberOfItemsOrdered;
            }
            function increment() {
                var value = numberOfItemsOrdered;

                $("input[name='numBooksOrdered']").val(value);
            }
            $('#submitOrder').click(function() {
                $('#completeOrder').attr('action', 'Order?number=' + numberOfItemsOrdered);
            })

        </script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Book Store</title>
    </head>
    <body>

        <div id="container">
            <div id="header">
                <h1>
                    JRS-BookStore</h1>
            </div>
            <div id="wrapper">
                <div id="content">
                    <c:forEach var="Book" items="#{bookList}">

                        <div id="bookDiv">
                            <form id="book" onsubmit="return false">
                                <table id="book">
                                    <tr>
                                        <td><img src="imgs/${Book.bookId}.jpeg"></td>
                                    </tr>
                                    <tr>
                                        <td id="bookName">${Book.bookName}</td>
                                    </tr>
                                    <tr>
                                        <td id="bookDesc">${Book.bookDesc}</td>
                                    </tr>
                                    <tr>
                                        <td id="bookPrice">${Book.bookPrice}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="submit" value="Add to Order" name="addBook" onclick="addToOrder('${Book.bookId}', '${Book.bookName}', '${Book.bookPrice}'), increment();
                                                   " Id=${Book.bookId}></td>
                                    </tr>
                                </table>
                            </form>
                        </div>

                    </c:forEach>

                </div>
                <div id="navigation">
                    <p>
                    <form method="POST" action="BookStore">
                        <input type="submit" id="BookStore" name="BookStore" value="BookStore">
                    </form>
                    <form method="POST" action="Admin">
                        <input type="submit" id="Admin" name="Admin" value="Admin">
                    </form>
                    </p>
                </div>
                <div id="extra">
                    <p>
                    <div id="orderContainer">

                        <form id="completeOrder" action="Order" method="POST">
                            <div id="order">
                                Your Cart:
                            </div>
                            <div id="customerInfo">
                                <table>
                                    <tr>First Name: <input type="text" name="fName" id="fName"></tr><br/>
                                    <tr>Last Name:<input type="text" name="lName" id="lName"></tr><br/>
                                    <tr>Address:<input type="text" name="address" id="address"></tr><br/>
                                    <tr>City: <input type="text" name="city" id="city"></tr><br/>
                                    <tr>State: <input type="text" name="state" id="state"></tr><br/>
                                    <tr>Zip Code: <input type="text" name="zipCode" id="zipCode"></tr><br/>

                            </div>
                            <input type="hidden" value="0" id="numBooksOrdered" name="numBooksOrdered">
                            <input type="submit" id="submitOrder" name ="submitOrder" value="Submit Order">
                            </table>
                            </div>
                        </form>
                    </div>
                </p>
            </div>
            <div id="footer">
                <p>
                    <a href="mailto:${email}">Email Me @: ${email}</a></p>
            </div>
        </div>
    </body>
</html>
