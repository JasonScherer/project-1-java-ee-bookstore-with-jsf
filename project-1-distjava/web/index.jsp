<%-- 
    Document   : index
    Created on : Feb 17, 2014, 8:34:38 PM
    Author     : schereja
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head><link rel="stylesheet" type="text/css" href="main.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Book Store</title>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <h1>
                    JRS-BookStore</h1>
            </div>
            <div id="wrapper">
                <div id="content">
                    <p>
                   Welcome to JRS-BookStore.  This web Application is a basic book store that offers the ability to add/delete/update and search for books.
                   Along with that it offers the ability to "order" books and save the information to a database.
                    ﻿</p>
                </div>
            </div>
            <div id="navigation">
                Please Select where you would like to go:
                <p>
                    <form method="POST" action="BookStore">
                        <input type="submit" id="BookStore" name="BookStore" value="BookStore">
                    </form>
                    <form method="POST" action="Admin">
                        <input type="submit" id="Admin" name="Admin" value="Admin">
                    </form>
                </p>
           </div>
            <div id="extra">
                <p>
                <ul>
                    <li>Features in application:</li>
                    <ul>
                        <li>Adding of Books</li>
                        <li>Deleting of Books</li>
                        <li>Updating of Books</li>
                        <li>Using CSS for basic layout</li>
                        <li>Search books</li>
                        <li>Place orders</li>
                    </ul>
                       
                </ul>
                   
                    
                </p>
            </div>
            <div id="footer">
                
            </div>
        </div>
    </body>
</html>
