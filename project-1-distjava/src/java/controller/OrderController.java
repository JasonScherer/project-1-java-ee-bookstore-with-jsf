/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Book;
import model.Customer;

/**
 *
 * @author schereja
 */
public class OrderController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        model.OrderService os = new model.OrderService("model.OrderDAO");
        model.BookStoreService bs = new model.BookStoreService("model.BookDAO");
       
        List<Book> orderedBooks = new ArrayList<>();
        Customer customer = new Customer();
        Book book = new Book();
        double finalPrice = 0.0;
        String fullAddress = "";
        String email="";
        ServletContext sc = request.getServletContext();
        email = sc.getInitParameter("email");
        request.setAttribute("email", email);
        HttpSession session = request.getSession(true);
        try (PrintWriter out = response.getWriter()) {
            if ("Submit Order".equals(request.getParameter("submitOrder"))) {
                
                
                String sNumBooks = request.getParameter("numBooksOrdered");
                int numBooks = Integer.parseInt(sNumBooks);
                customer.setCustomerId(-1);
                customer.setfName(request.getParameter("fName"));
                customer.setlName(request.getParameter("lName"));
                customer.setAddress(request.getParameter("address"));
                customer.setCity(request.getParameter("city"));
                customer.setState(request.getParameter("state"));
                customer.setZipCode(Integer.parseInt(request.getParameter("zipCode")));
                fullAddress = request.getParameter("fName") + " " + request.getParameter("lName")
                        + "<br />" + request.getParameter("address") + "<br />" + request.getParameter("city") + ", "
                        + request.getParameter("state") + " " + request.getParameter("zipCode") + "<br/>";
                for (int i = 0; i < numBooks; i++) {
                    String paraId = request.getParameter("book" + i);
                    int bookId = Integer.parseInt(paraId);
                    book = bs.getBookDao().findBookById(bookId);
                    finalPrice += book.getBookPrice();
                    orderedBooks.add(book);
                }
                finalPrice = finalPrice * (1.065);
                session.setAttribute("finalPrice", finalPrice);
                session.setAttribute("books", orderedBooks);
                session.setAttribute("fullAddress", fullAddress);
                session.setAttribute("customer", customer);
                request.setAttribute("fullAddress", fullAddress);
                request.setAttribute("orderedBooks", orderedBooks);
               
                RequestDispatcher dispatcher
                        = getServletContext().getRequestDispatcher("/confirmation.jsp");
                dispatcher.forward(request, response);
            }
            if ("Confirm Order".equals(request.getParameter("submitOrder"))) {
               
                fullAddress = (String) session.getAttribute("fullAddress");
                finalPrice = (double) session.getAttribute("finalPrice");
                customer = (Customer) session.getAttribute("customer");
                orderedBooks = (List<Book>) session.getAttribute("books");
                
                customer.setCc(request.getParameter("cc"));
                os.setCustomer(customer);
                os.setBooksOrdered(orderedBooks);
                os.submitOrder();
                
                
                RequestDispatcher dispatcher
                        = getServletContext().getRequestDispatcher("/orderSubmitted.jsp");
                dispatcher.forward(request, response);

            }

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
