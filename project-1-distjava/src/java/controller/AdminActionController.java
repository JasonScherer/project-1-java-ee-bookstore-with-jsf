
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Book;
import model.BookStoreService;
import model.IBookDAO;

/**
 * Used for all admin features
 * 
 * @author schereja
 */
public class AdminActionController extends HttpServlet {
   
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email="";
        ServletContext sc = request.getServletContext();
        email = sc.getInitParameter("email");
        request.setAttribute("email", email);
        String action = request.getParameter("action");
        Book book = new Book();
        String bookAdded = "";
        String bookEdited ="";
        response.setContentType("text/html;charset=UTF-8");
        model.BookStoreService bs = new BookStoreService("model.BookDAO");
      //  List<Book> books = bs.getBookList();
        IBookDAO dao = bs.getBookDao();
        try (PrintWriter out = response.getWriter()) {
            if ("edit".equals(action)) {
                int bookId = Integer.parseInt(request.getParameter("selectedBook"));
                book= dao.findBookById(bookId);
                request.setAttribute("bookEdited", bookEdited);
                request.setAttribute("book", book);
                RequestDispatcher dispatcher
                        = getServletContext().getRequestDispatcher("/edit.jsp");
                dispatcher.forward(request, response);
            }
            if ("add".equals(action)) {
                request.setAttribute("bookAdded", bookAdded);
                RequestDispatcher dispatcher
                        = getServletContext().getRequestDispatcher("/add.jsp");
                dispatcher.forward(request, response);
            }
            if ("delete".equals(action)) {
                int bookId = Integer.parseInt(request.getParameter("selectedBook"));
                book= dao.findBookById(bookId);
                dao.deleteBook(book);
            }
            if ("newBook".equals(action)){
                book = new Book(-1, request.getParameter("bookTitle").toString(), request.getParameter("bookDescription").toString(), Double.parseDouble(request.getParameter("bookPrice")));
                dao.addUpdateBook(book);
                
                bookAdded = "Book " + book.getBookName() + " has been added.";
                request.setAttribute("bookAdded", bookAdded);
                RequestDispatcher dispatcher
                        = getServletContext().getRequestDispatcher("/add.jsp");
                dispatcher.forward(request, response);
            }
            if ("editBook".equals(action)){
                request.setAttribute("book", book);
                bookEdited = "Book " + request.getParameter("editBookName") + " has been edited.";
                request.setAttribute("bookEdited", bookEdited);
                book = new Book(Integer.parseInt(request.getParameter("editBookId").toString()), request.getParameter("editBookName").toString(), request.getParameter("editBookDesc").toString(), Double.parseDouble(request.getParameter("editBookPrice")));
                dao.addUpdateBook(book);
                RequestDispatcher dispatcher
                        = getServletContext().getRequestDispatcher("/index.jsp");
                dispatcher.forward(request, response);
            }
            if ("findBook".equals(action)){
                
                book = dao.findBookByValue(request.getParameter("searchField"), request.getParameter("searchValue"));
                request.setAttribute("foundBook", book);
                 RequestDispatcher dispatcher
                        = getServletContext().getRequestDispatcher("/searchResult.jsp");
                dispatcher.forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
