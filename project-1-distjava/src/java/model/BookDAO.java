package model;

import db.accessor.DB_Generic;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * DAO used for Books
 *
 * @author schereja
 */
public class BookDAO implements IBookDAO {

    private db.accessor.DB_Generic db;
    private final String TABLE_NAME = "Books";
    private final   String BOOK_COLUMNS[] = {"Book_Id", "Book_name", "Book_Desc", "Book_Price"};
    private final  List<String> colNames = new ArrayList<>();
    private final static String BOOK_ID = "Book_Id";
    private final static String BOOK_NAME = "Book_Name";
    private final static String BOOK_DESC = "Book_Desc";
    private final static String BOOK_PRICE = "Book_Price";
    private final static String DB_ERROR = "Db does not exist.";
    private final static String DRIVER_CLASS = "com.mysql.jdbc.Driver";
    private final static String URL = "jdbc:mysql://localhost/bookstore";
    private final static String USER = "root";
    private final static String PASSWORD = "password";
    private final static String SQL_ERROR = "SQL Statement is invalid";
    private final static String BOOK_ERROR = "Book value is null.";
    private final static String FOUND_ERROR = "Inputs are null";
    private final static String ID_ERROR = "ID is below 0, value must be above 0.";

    /**
     * Basic constructor for BookDAO. Automatically sets the database and adds
     * all the column names to the colNames List
     *
     */
    public BookDAO() {
        this.db = new DB_Generic();
        for (String BOOK_COLUMNS1 : BOOK_COLUMNS) {
            colNames.add(BOOK_COLUMNS1);
        }
    }

    /**
     * Used to set the db to be different then DB_Generic
     *
     * @param db Requires a DB_Generic object
     * @throws IllegalArgumentException when DB is incorrect
     */
    public BookDAO(db.accessor.DB_Generic db) {
        if (db == null) {
            throw new IllegalArgumentException(DB_ERROR);
        } else {
            this.db = db;
            colNames.addAll(Arrays.asList(BOOK_COLUMNS));
        }

    }

    /**
     * Used to connect to the test database based on driver class, url, user and
     * password
     *
     */
    private void openTestDbConnection() {
        try {
            db.openConnection(DRIVER_CLASS, URL, USER, PASSWORD);
        } catch (IllegalArgumentException ex) {
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Adds or updates a book based on if book exists, if doesn't exist will add
     * the book to the database
     *
     *
     * @param book Requires a Book object
     * @throws SQLException when SQL statement is invalid
     */
    @Override
    public void addUpdateBook(Book book) {
        if (book == null) {
            throw new IllegalArgumentException(BOOK_ERROR);
        } else {
            this.openTestDbConnection();
            List values = new ArrayList();
            values.add(book.getBookId());
            values.add(book.getBookName());
            values.add(book.getBookDesc());
            values.add(book.getBookPrice());
            try {
                if (book.getBookId() < 0) {
                    colNames.remove(0);
                    values.remove(0);
                    db.insertRecord(TABLE_NAME, colNames, values, Boolean.TRUE);
                } else {
                    db.updateRecord(TABLE_NAME, colNames, values, BOOK_ID, book.getBookId(), Boolean.TRUE);
                }
            } catch (SQLException sqlEx) {
                System.out.println(SQL_ERROR);
            }
        }
    }

    /**
     * Used to Delete a Book object
     *
     *
     * @param book Requires a Book object to be used to be deleted
     * @throws SQLException when SQL statement is invalid
     */
    @Override
    public void deleteBook(Book book) {
        if (book != null) {
            this.openTestDbConnection();
            try {
                db.deleteRecord(TABLE_NAME, BOOK_ID, book.getBookId(), Boolean.TRUE);
            } catch (SQLException ex) {
                System.out.println(SQL_ERROR);
            }
        } else {
            throw new IllegalArgumentException(BOOK_ERROR);
        }

    }

    /**
     * Finds a book by a certain value
     *
     * @param columnName - Requires a String of the column name
     * @param value - Requires an Object for the value that will be searched for
     * @return Returns a Book object
     *
     */
    @Override
    public Book findBookByValue(String columnName, Object value) {
            this.openTestDbConnection();
            Book book = new Book();
            Map record = new HashMap();
            try {
                record = db.findRecordByValue(TABLE_NAME, colNames, columnName, value);
            } catch (SQLException sqlEx) {
                System.out.println(sqlEx.getMessage());
            }
            book.setBookId((Integer) record.get("Book_Id"));
            book.setBookName(record.get(BOOK_NAME).toString());
            book.setBookDesc(record.get(BOOK_DESC).toString());
            book.setBookPrice(Double.parseDouble(record.get(BOOK_PRICE).toString()));
            return book;
       

    }

    /**
     * Allows you to find a book by an ID
     *
     *
     * @param id Requires an int value
     * @return Book object
     * @throws IllegalArgumentException when ID is 0 or below.
     */
    @Override
    public Book findBookById(int id) {
        if (id <= 0) {
            throw new IllegalArgumentException(ID_ERROR);
        } else {
            this.openTestDbConnection();
            Book book = new Book();
            Map record = new HashMap();
            try {
                record = db.findRecordById(TABLE_NAME, colNames, BOOK_ID, id);
            } catch (SQLException ex) {
                Logger.getLogger(BookDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            book.setBookId((Integer) record.get(BOOK_ID));
            book.setBookName(record.get(BOOK_NAME).toString());
            book.setBookDesc(record.get(BOOK_DESC).toString());
            book.setBookPrice(Double.parseDouble(record.get(BOOK_PRICE).toString()));
            return book;
        }
    }

    /**
     * Retrieves all books from inventory
     *
     * @return Returns a list of Book objects to use inventory.
     */
    @Override
    public List<Book> getAllBooks() {
        this.openTestDbConnection();
        List<Map> rawData = new ArrayList<>();
        List<Book> records = new ArrayList<>();
        try {
            rawData = db.findRecords(TABLE_NAME, colNames, BOOK_ID, Boolean.TRUE);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        Book book = null;
        for (Map data : rawData) {
            book = new Book();
            String id = data.get(BOOK_ID).toString();
           
            book.setBookId(new Integer(id));
            String name = data.get(BOOK_NAME).toString();
            book.setBookName(name);
            String desc = data.get(BOOK_DESC).toString();
            book.setBookDesc(desc);
            String price = data.get(BOOK_PRICE).toString();
            book.setBookPrice(Double.parseDouble(price));
            records.add(book);
        }
        return records;
    }

    public static void main(String[] args) {
        BookDAO b = new BookDAO(new DB_Generic());
        //System.out.println(b.findBookByValue("Book_Name", "1984"));
        System.out.println(b.getAllBooks());

        //System.out.println(b.findBookById(2));
    }
}
