
package model;

import java.util.List;

/**
 * Used as an interface for the BookDAOs
 * @author schereja
 */
public interface IBookDAO {
    public abstract void addUpdateBook(Book book);
    public abstract void deleteBook(Book book);
    public abstract Book findBookByValue(String columnName, Object Value);
    public abstract Book findBookById(int id);
    public abstract List<Book> getAllBooks();
    
    
}
