package model;

/**
 * Basic Customer Object
 *
 * @author schereja
 */
public class Customer {

    private int customerId;
    private String fName;
    private String lName;
    private String address;
    private String city;
    private String state;
    private int zipCode;
    private String cc;
    private final static String ID_ERROR = "Int value must be above -1";
    private final static String FNAME_ERROR = "fName length must be above 0 and cannot be null.";
    private final static String LNAME_ERROR = "lName length must be above 0 and cannot be null.";
    private final static String ADDRESS_ERROR = "Address length must be above 0 and cannot be null.";
    private final static String CITY_ERROR = "City length must be above 0 and cannot be null.";
    private final static String STATE_ERROR = "State length must be above 0 and cannot be null.";
    private final static String ZIPCODE_ERROR = "Zip Code length must be 5 characters in length";
    private final static String CC_ERROR = "CC must not be null and cannot be below 0 characters";

    /**
     * Basic Constructor for the Customer Object
     *
     */
    public Customer() {

    }

    /**
     * Used to create a Customer with all the properties filled out
     *
     * @param customerId Requires an Int value for the customer ID
     * @param fName Requires a String value for the first Name
     * @param lName Requires a String value for the Last Name
     * @param address Requires a String value for the Address
     * @param city Requires a String value for the City
     * @param state Requires a String value for the state
     * @param zipCode Requires an int value for the ZipCode
     * @param cc Requires a String value for the credit card
     *
     */
    public Customer(int customerId, String fName, String lName, String address, String city, String state, int zipCode, String cc) {
        setCustomerId(customerId);
        setfName(fName);
        setlName(lName);
        setAddress(address);
        setCity(city);
        setState(state);
        setZipCode(zipCode);
        setCc(cc);

    }

    /**
     * Getter for the customer ID
     *
     * @return Returns an int value of the customer ID
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     * Setter for the customer ID
     *
     * @param customerId Requires an INT value for the customer ID
     * @throws IllegalArgumentException when value is below -1
     */
    public void setCustomerId(int customerId) {
        if (customerId < -1) {
            throw new IllegalArgumentException(ID_ERROR);
        } else {
            this.customerId = customerId;
        }
    }

    /**
     * Getter for the first Name
     *
     * @return Returns a string for the first name
     */
    public String getfName() {
        return fName;
    }

    /**
     * Setter for the first Name
     *
     * @param fName Requires a String value that is equal to the first name
     * @throws IllegalArgumentException when string is null or length is 0
     */
    public void setfName(String fName) {
        if (fName == null || fName.length() == 0) {
            throw new IllegalArgumentException(FNAME_ERROR);
        } else {
            this.fName = fName;
        }
    }

    /**
     * Getter for the last name String
     *
     * @return Returns String value for the last name
     */
    public String getlName() {
        return lName;
    }

    /**
     * Setter for the Last Name
     *
     * @param lName Requires a String for the last name
     * @throws IllegalArgumentException when string is null or length is 0
     */
    public void setlName(String lName) {
        if (lName.length() == 0 || lName == null) {
            throw new IllegalArgumentException(LNAME_ERROR);
        } else {
            this.lName = lName;
        }
    }

    /**
     * Getter used for getting the address
     *
     * @return Returns a string value for the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Setter for the address
     *
     * @param address Requires a String value for the address
     * @throws IllegalArgumentException when string is null or length is 0
     */
    public void setAddress(String address) {
        if (address.length() == 0 || address == null) {
            throw new IllegalArgumentException(ADDRESS_ERROR);
        } else {
            this.address = address;
        }
    }

    /**
     * Getter for the city value
     *
     * @return Returns a String for the City
     */
    public String getCity() {
        return city;
    }

    /**
     * Setter for the city
     *
     * @param city Requires a String for the city value
     * @throws IllegalArgumentException when string is null or length is 0
     */
    public void setCity(String city) {
        if (city == null || city.length() == 0) {
            throw new IllegalArgumentException(CITY_ERROR);

        } else {
            this.city = city;
        }
    }

    /**
     * Getter for the state value
     *
     * @return Returns a String for the state
     */
    public String getState() {
        return state;
    }

    /**
     * Setter for the state property
     *
     * @param state Requires a String for the state value
     * @throws IllegalArgumentException when string is null or length is 0
     */
    public void setState(String state) {
        if (state.length() == 0 || state == null) {
            throw new IllegalArgumentException(CITY_ERROR);
        } else {
            this.state = state;
        }
    }

    /**
     * Getter for the zip code property
     *
     * @return Returns an int value for the zip code
     */
    public int getZipCode() {
        return zipCode;
    }

    /**
     * Setter for the zip code property
     *
     * @param zipCode Requires an int value for the zipcode
     *
     */
    public void setZipCode(int zipCode) {

        this.zipCode = zipCode;
    }

    /**
     * Getter for the credit card
     *
     * @return Returns a string value of the Credit card
     */
    public String getCc() {
        return cc;
    }

    /**
     * Setter for the Credit card
     *
     * @param cc Requires a String value for the credit card
     * @throws IllegalArgumentException when string is null or length is 0
     */
    public void setCc(String cc) {
        if (cc.length() == 0) {
            throw new IllegalArgumentException(CC_ERROR);
        } else {
            this.cc = cc;
        }
    }

}
