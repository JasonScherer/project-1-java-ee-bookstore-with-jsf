
package model;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

/**
 *  Service used for Orders
 * 
 * @author schereja
 */
public class OrderService {
    private List<Book> bookList;
    private List<Book> booksOrdered;
    private model.OrderDAO orderDAO;
    private Customer customer;
    private static final String DAO_ERROR = "DAO String is null or length is <= 0";
    private static final String BOOKLIST_ERROR ="List of books is null.";
    private static final String ORDERDAO_ERROR ="Order Dao is null.";
    private static final String CUST_ERROR = "Customer object is null.";
    private static final String BOOKS_ORDERED_ERROR ="Books ordered list is null.";
    
            
    /**
     * Basic constructor
     * 
     */
    public OrderService(){
        
    }
    /**
     * Basic constructor to set DAO and init database
     * 
     * @param dao Requires a String for the DAO being used
     * @throws IllegalArgumentException when string is null or length is <= 0
     */
    
    public OrderService(String dao){
        if(dao == null || dao.length() <= 0){
            throw new IllegalArgumentException(DAO_ERROR);
        } else{
        initBookDb(dao);
        }
    }
    /**
 * Initializes the database being used by the DAO 
 * 
 * @param dao 
 * @throws IllegalArgumentException when string is null or length is <= 0
 */
    private void initBookDb(String dao){
        if(dao == null || dao.length() <= 0){
            throw new IllegalArgumentException(DAO_ERROR);
        } else{
        Class clazz = null;
        try {
            clazz = Class.forName(dao);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(OrderService.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            orderDAO = (OrderDAO)(IOrderDAO)clazz.newInstance();
        } catch (InstantiationException ex) {
            Logger.getLogger(OrderService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(OrderService.class.getName()).log(Level.SEVERE, null, ex);
        }
        bookList = orderDAO.getBookChoices();
        booksOrdered = new ArrayList<>();}
    }
    /**
     * 
     * Submits the order by entering the customer and the books order into the database
     */
    public void submitOrder(){
        orderDAO.enterCustomer(customer);
        orderDAO.saveOrder(booksOrdered);
        bookList = orderDAO.getBookChoices();
        booksOrdered = new ArrayList<>();
    }
    /**
     * Getter for the book list property
     * 
     * @return Returns a list of books 
     */
    public List<Book> getBookList() {
        return bookList;
    }
/**
 * Setter for the book list property.
 * 
 * @param bookList Requires a list of book objects
 * @throws IllegalArgumentException when list of books is null
 */
    public void setBookList(List<Book> bookList) {
        if(bookList == null){
            throw new IllegalArgumentException(BOOKLIST_ERROR);
        } else{
        this.bookList = bookList;
        }
    }
/**
 * Getter for the books ordered list
 * 
 * @return Returns a list of book objects
 */
    public List<Book> getBooksOrdered() {
        return booksOrdered;
    }
/**
 * Setter for the books ordered
 * 
 * @param booksOrdered requires a list of Book objects ordered
 * @throws IllegalArgumentException when booksOrdered is null
 */
    public void setBooksOrdered(List<Book> booksOrdered) {
        if (booksOrdered == null){
            throw new IllegalArgumentException(BOOKS_ORDERED_ERROR);
        } else{
        this.booksOrdered = booksOrdered;}
        
    }
/**
 * Getter for the Order DAO
 * 
 * @return returns an orderDAO object
 */
    public OrderDAO getOrderDAO() {
        return orderDAO;
    }
/**
 * Setter for ORDER DAO
 * 
 * @param orderDAO Requires an OrderDAO object
 * @throws IllegalArgumentException when orderDAO is null
 */
    public void setOrderDAO(OrderDAO orderDAO) {
        if(orderDAO == null){
            throw new IllegalArgumentException(ORDERDAO_ERROR);
        } else{
        this.orderDAO = orderDAO;
        }
    }
/**
 * Getter for the customer object
 * 
 * @return Returns a customer object
 */
    public Customer getCustomer() {
        return customer;
    }
/**
 * Setter for Customer
 * 
 * @param customer Requires a Customer Object
 */
    public void setCustomer(Customer customer) {
        if (customer == null){
            throw new IllegalArgumentException(CUST_ERROR);
        } else{
        this.customer = customer;
        }
    }  
    
    public static void main(String[] args) {
        OrderService os = new OrderService("model.OrderDAO");
        List<Book> test = new ArrayList<>();
        Book book = new Book(1, "testing", "testing", 12.99);
        Customer customer = new Customer(-1, "Jason", "Scherer", "1234", "West Allis", "WI", 53214, "82384-124214124-12");
        test.add(book);
        os.setCustomer(customer);
        os.setBookList(test);
        os.submitOrder();
    }

}
