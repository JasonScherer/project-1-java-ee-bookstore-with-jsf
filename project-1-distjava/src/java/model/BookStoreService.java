package model;

import java.util.List;

/**
 * Book Store service to be used to communicate with DAO and controller
 *
 * @author schereja
 */
public class BookStoreService {

    private List<Book> bookList;
    private model.IBookDAO bookDao;
    private final String DAO_ERROR = "DAO string is null or blank";
    private final String BOOK_LIST_ERROR = "The List of Book Objects is invalid";

    /**
     * Basic bookStoreService Constructor
     *
     */
    public BookStoreService() {

    }

    /**
     * Constructor used to pass a string of the DAO
     *
     * @param dao Requires a string of the DAO being used
     */
    public BookStoreService(String dao) {
        if (dao == null || dao.length() == 0) {
            throw new IllegalArgumentException(DAO_ERROR);
        } else {
            try {
                initBooksDb(dao);
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    /**
     * Used to initialize the books database
     *
     * @param dao requires a String of the DAO being used
     * @throws ClassNotFoundException Throws ClassNotFoundException when errored
     * @throws InstantiationException Throws an instantiationexception when
     * errored
     * @throws IllegalAccessException throws an illegalAccessException when
     * invalid
     */
    private void initBooksDb(String dao) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        if (dao == null || dao.length() == 0) {
            throw new IllegalArgumentException(DAO_ERROR);
        } else {
            Class clazz = Class.forName(dao);
            bookDao = (IBookDAO) clazz.newInstance();
            bookList = bookDao.getAllBooks();
        }
    }

    /**
     * Gets a list of Book objects
     *
     * @return Returns a list of Book objects
     */
    public List<Book> getBookList() {
        return bookList;
    }

    /**
     * Sets the list of Book objects
     *
     * @param bookList Requires a List of Book Objects
     * @throws IllegalArgumentException when the List of Books are invalid
     */
    public void setBookList(List<Book> bookList) {
        if (bookList != null && bookList.size() > 0) {
            this.bookList = bookList;
        } else {
            throw new IllegalArgumentException(BOOK_LIST_ERROR);
        }
    }

    /**
     * Getter for the IBookDAO
     *
     * @return Returns IBookDAO being used
     */
    public IBookDAO getBookDao() {
        return bookDao;
    }
    /**
     * Setter for the BOOKDAO being used
     * 
     * @param bookDao Requires a IBookDAO
     */
    public void setBookDao(IBookDAO bookDao) {
        this.bookDao = bookDao;
    }

    public static void main(String[] args) {
        BookStoreService bs = new BookStoreService("model.BookDAO");
        System.out.println(bs.getBookList());
    }
}
