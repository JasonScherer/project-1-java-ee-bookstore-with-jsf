
package model;

import java.util.List;

/**
 *  Used for the interface for the Order DAOs
 * @author schereja
 */
public interface IOrderDAO {
    public List<Book> getBookChoices();
    public void saveOrder(List<Book> orderList) throws RuntimeException;
    public void enterCustomer(Customer customer);
}
