package model;

import db.accessor.DB_Generic;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Order DAO to to communicate with the database for orders Next version if had
 * more time would separate the customer into it's own DAO
 *
 * @author schereja
 */
public class OrderDAO implements IOrderDAO {

    private DB_Generic db;
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL
            = "jdbc:mysql://localhost/bookstore";
    private static final String USER = "root";
    private static final String PWD = "password";
    private static int customerId = 1;
    private static final String BOOK_TABLE_NAME = "Books";
    private static final String BOOK_SORT_COLUMN = "Book_Id";
    private static final String BOOK_COLUMNS[] = {"book_id", "Book_name", "Book_Desc", "Book_Price"};
    private List<String> bookColNames = new ArrayList<>();
    private static final boolean SORT_ASC = true;
    private static final String ORDER_TABLE_NAME = "Orders";
    private static final String[] ORDER_COLUMNS = {"Order_Price", "Order_Date", "Customer_Id", "Invoice"};
    private List<String> orderColNames = new ArrayList<String>();
    private static final String CUSTOMER_TABLE = "customer";
    private static final String[] CUSTOMER_COLUMNS = {"customer_Id", "fName", "lName", "address", "city", "state", "zipCode", "cc"};
    private List<String> customerColNames = new ArrayList<String>();
    private static final String CUSTOMER_CC_COLUMN = "cc";
    private final String CUSTOMER_ID = "customer_Id";
    private Customer customer;
    private static final String BOOKS_LIST_ERROR = "List of books is invalid";
    private static final String CUSTOMER_ERROR = "Customer is invalid";
    private static final String SQL_ERROR = "SQL statement is invalid";
    private static final String DB_ERROR = "DB_Generic is invalid";
    private static final String ID_ERROR = "Customer ID value is invalid.";
    private static final String COLUMN_ERROR = "Column list is null";
    private static final String ORDER_COL_ERROR = "Order Column list of strings is null";
    private static final String CUST_COL_NAMES_ERROR = "List of Strings is null";
    private static final String CUST_ERROR = "Customer value is null";

    /**
     * Constructor to set up the database being used and to add book columns to
     * the list of orderColNames
     *
     */
    public OrderDAO() {
        db = new DB_Generic();
    }

    /**
     * Opens a database connection to the test database
     *
     */
    private void openTestDbConnection() {
        try {
            db.openConnection("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/bookstore", "root", "password");
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(BookDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BookDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(BookDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Gets all the book choices that are available for the bookstore
     *
     * @return Returns a list of book objects
     */
    @Override
    public List<Book> getBookChoices() {
        List<Book> books = new ArrayList<>();
        for (int i = 0; i < BOOK_COLUMNS.length; i++) {
            bookColNames.add(BOOK_COLUMNS[i]);
        }
        try {
            db.openConnection(DRIVER, URL, USER, PWD);
            List<Map> rawData = db.findRecords(BOOK_TABLE_NAME, bookColNames, BOOK_SORT_COLUMN, SORT_ASC);
            for (Map record : rawData) {
                Book book = new Book();
                int id = Integer.valueOf(record.get("Book_Id").toString());
                book.setBookId(id);
                String name = String.valueOf(record.get("Book_Name"));
                book.setBookName(name);
                String desc = String.valueOf(record.get("Book_Desc"));
                book.setBookDesc(desc);
                Double price = Double.valueOf(record.get("Book_Price").toString());
                book.setBookPrice(price);
                books.add(book);
            }

        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return books;
    }

    /**
     * Used to save the order based on the books ordered Must be ran after enter
     * customer
     *
     * @param booksOrdered Requires a list of books for the order
     * @throws RuntimeException Throws error when settings are incorrect
     */
    @Override
    public void saveOrder(List<Book> booksOrdered) throws RuntimeException {
        if (booksOrdered == null) {
            throw new IllegalArgumentException(BOOKS_LIST_ERROR);
        } else {
            this.openTestDbConnection();
            for (int i = 0; i < ORDER_COLUMNS.length; i++) {
                orderColNames.add(ORDER_COLUMNS[i]);
            }
            try {

                double finalPrice = 0;
                String bookValues = "Books ordered: ";
                List<Object> values = new ArrayList<>();
                Date date = new Date();
                for (Book book : booksOrdered) {
                    bookValues += book.getBookName() + " " + book.getBookPrice() + "<br /> ";
                    finalPrice += book.getBookPrice();
                }
                values.add(finalPrice);
                values.add(date);
                values.add(this.customer.getCustomerId());
                values.add(bookValues);
                db.insertRecord(ORDER_TABLE_NAME, orderColNames, values, SORT_ASC);

            } catch (SQLException sqlEx) {
                System.out.println(sqlEx.getMessage());
            }
        }

    }

    /**
     * Enters a new customer
     *
     * @param newCustomer Requires a Customer Object to be used to enter a new
     * customer
     * @Throws illegalArguementException when customer object is invalid
     */
    @Override
    public void enterCustomer(Customer newCustomer) {
        if (newCustomer == null) {
            throw new IllegalArgumentException(CUSTOMER_ERROR);
        } else {
            for (int i = 0; i < CUSTOMER_COLUMNS.length; i++) {
                customerColNames.add(CUSTOMER_COLUMNS[i]);
            }
            this.openTestDbConnection();
            List values = new ArrayList();
            values.add(newCustomer.getCustomerId());
            values.add(newCustomer.getfName());
            values.add(newCustomer.getlName());
            values.add(newCustomer.getAddress());
            values.add(newCustomer.getCity());
            values.add(newCustomer.getState());
            values.add(newCustomer.getZipCode());
            values.add(newCustomer.getCc());

            try {
                if (newCustomer.getCustomerId() < 0) {
                    customerColNames.remove(0);
                    values.remove(0);
                    db.insertRecord(CUSTOMER_TABLE, customerColNames, values, SORT_ASC);
                    customerColNames.add(0, CUSTOMER_ID);
                    this.openTestDbConnection();
                    int customerID = Integer.parseInt(db.findRecordByValue(CUSTOMER_TABLE, customerColNames, CUSTOMER_CC_COLUMN, newCustomer.getCc()).get(CUSTOMER_ID).toString());
                    this.setCustomer(newCustomer);
                    newCustomer.setCustomerId(customerID);
                }
            } catch (SQLException ex) {
                throw new IllegalArgumentException(SQL_ERROR);
            }
        }
    }

    /**
     * Getter for the DB object being used
     *
     * @return Returns the database object
     */
    public DB_Generic getDb() {
        return db;
    }

    /**
     * Setter for the DB object
     *
     * @param db Requires a DB_Generic object
     */
    public void setDb(DB_Generic db) {
        if (db == null) {
            throw new IllegalArgumentException(DB_ERROR);
        } else {
            this.db = db;
        }
    }

    /**
     * Getter for the customer ID
     *
     * @return Returns an int value for the customer ID
     */
    public static int getCustomerId() {
        return customerId;
    }

    /**
     * Setter for the customer ID
     *
     * @param customerId Requires an int value
     * @throws IllegalArgumentException when ID is below -1
     */
    public static void setCustomerId(int customerId) {
        if (customerId < -1) {
            throw new IllegalArgumentException(ID_ERROR);
        } else {
            OrderDAO.customerId = customerId;
        }
    }

    /**
     * Getter for the book column names
     *
     * @return Returns a list of Strings
     */
    public List<String> getBookColNames() {
        return bookColNames;
    }

    /**
     * Setter for the book column names
     *
     *
     * @param bookColNames Requires a list of Strings for the book column names
     * @throws IllegalArgumentException throws illegal argument exception when
     * list of Strings are null
     */
    public void setBookColNames(List<String> bookColNames) {
        if (bookColNames == null) {
            throw new IllegalArgumentException(COLUMN_ERROR);
        } else {
            this.bookColNames = bookColNames;
        }
    }

    /**
     * Getter for order column names
     *
     * @return Returns a list of Strings for the order column names
     */
    public List<String> getOrderColNames() {
        return orderColNames;
    }

    /**
     * Setter for order column names
     *
     * @param orderColNames Requires a list of strings of the order column names
     * @throws IllegalArgumentException when list of strings is null
     */
    public void setOrderColNames(List<String> orderColNames) {
        if (orderColNames == null) {
            throw new IllegalArgumentException(ORDER_COL_ERROR);
        } else {
            this.orderColNames = orderColNames;
        }

    }

    /**
     * Getter for the customer column names
     *
     * @return Returns a list of String values for the customer column names
     */
    public List<String> getCustomerColNames() {
        return customerColNames;
    }

    /**
     * Setter for the customer col names. Has to be a list of Strings
     *
     *
     * @param customerColNames List of Strings for the customer column names
     * @throws IllegalArgumentException when list of Strings is null
     */
    public void setCustomerColNames(List<String> customerColNames) {
        if (customerColNames == null) {
            throw new IllegalArgumentException(CUST_COL_NAMES_ERROR);
        } else {
            this.customerColNames = customerColNames;
        }

    }

    /**
     * Getter for the customer Object
     *
     * @return Returns a customer Object
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Setter for the customer Object
     *
     * @param customer Requires a customer Object
     * @throws IllegalArgumentException when customer value is null
     */
    public void setCustomer(Customer customer) {
        if (customer == null) {
            throw new IllegalArgumentException(CUST_ERROR);
        } else {

            this.customer = customer;
        }

    }

    public static void main(String[] args) {
        OrderDAO odao = new OrderDAO();
        List<Book> test = new ArrayList<>();
        Book book = new Book(1, "testing", "testing", 12.99);
        test.add(book);
        Customer customer = new Customer(-1, "Jason", "Scherer", "1234", "West Allis", "WI", 53214, "1234");
        odao.enterCustomer(customer);
        odao.saveOrder(test);

    }
}
