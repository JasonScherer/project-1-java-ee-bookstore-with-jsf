package model;

/**
 * This creates an entity object for use with a database.
 *
 * @author schereja
 */
public class Book {

    private int bookId;
    private String bookName;
    private String bookDesc;
    private double bookPrice;
    private static final String ID_ERROR = "ID cannot equal 0.";
    private static final String NAME_ERROR = "Name is needed.";
    private static final String DESC_ERROR = "Description is needed.";
    private static final String PRICE_ERROR = "Price must be 0.00 or greater.";
    private static final String BLANK = "";
   
    /**
     * Basic constructor for a book
     *
     */
    public Book() {

    }

    /**
     * Book constructor to create a book object with ID, Name, Description, and
     * Price
     *
     * @param id Int value of the book ID
     * @param name String value for the name of the book
     * @param desc String value for the description of the book
     * @param price Double value of the price of the book
     */
    public Book(int id, String name, String desc, double price) {
        setBookId(id);
        setBookName(name);
        setBookDesc(desc);
        setBookPrice(price);
    }

    /**
     * Getter for the book's ID.
     *
     * @return Returns an int value for the book ID
     */
    public int getBookId() {
        return bookId;
    }

    /**
     * Setter for the book's ID, requires an int value for the book id.
     *
     *
     * @param bookId Requires an Int value that is above 0
     * @throws IllegalArgumentException when value is not above 0
     */
    public void setBookId(int bookId) {
        if (bookId != 0) {
            this.bookId = bookId;
        } else {
            throw new IllegalArgumentException(ID_ERROR);
        }
    }

    /**
     * Getter for the book's name
     *
     *
     * @return Returns a string value for the book's name
     */
    public String getBookName() {
        return bookName;
    }

    /**
     * Setter for Book's name
     *
     * @param bookName Requires a String value for the book's name
     * @throws IllegalArgumentException Throws exception when String is null
     */
    public void setBookName(String bookName) {
        if (bookName != null && !BLANK.equals(bookName)) {
            this.bookName = bookName;
        } else {
            throw new IllegalArgumentException(NAME_ERROR);
        }

    }

    /**
     * Getter for book's description
     *
     * @return Returns a string value of the book's description.
     */
    public String getBookDesc() {
        return bookDesc;
    }

    /**
     * Setter for book's description.
     *
     * @param bookDesc Requires a string value for the book's description
     * @throws IllegalArgumentException Throws exception when book's description
     * is null or blank.
     */
    public void setBookDesc(String bookDesc) {
        if (bookDesc != null && !BLANK.equals(bookDesc)) {
            this.bookDesc = bookDesc;
        } else {
            throw new IllegalArgumentException(DESC_ERROR);
        }
    }

    /**
     * Getter for book's price
     *
     * @return Returns a double value of the price of the book
     */
    public double getBookPrice() {
        return bookPrice;
    }

    /**
     * Setter for the book's price
     *
     * @param bookPrice Requires a double value for the price of the book
     * @throws IllegalArgumentException Throws exception when value is below 0
     */
    public void setBookPrice(double bookPrice) {
        if (bookPrice > 0.00) {
            this.bookPrice = bookPrice;
        } else {
            throw new IllegalArgumentException(PRICE_ERROR);
        }
    }

    /**
     * Creates a hashcode based on the bookId
     *
     * @return Returns an int of the hashcode
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + this.bookId;
        return hash;
    }

    /**
     * Equals method to check bookId
     *
     * @param obj Requires an object
     * @return Returns a boolean if they are equal or not
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Book other = (Book) obj;
        if (this.bookId != other.bookId) {
            return false;
        }
        return true;
    }

    /**
     * Override of the ToString method
     *
     * @return Returns a string value of all the values in the book object
     */
    @Override
    public String toString() {
        return "Book{" + "bookId=" + this.bookId + ", bookName=" + bookName + ", bookDesc=" + bookDesc + ", bookPrice=" + bookPrice + '}';
    }
    public static void main(String[] args) {
        Book book = new Book(1, "test", "Test", 12.99);
        System.out.println(book);
    }
}
