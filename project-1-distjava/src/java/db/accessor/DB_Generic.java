package db.accessor;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Basic Database class used for connecting to a MySQL database.
 *
 * @version 1.0
 * @author schereja
 */
public class DB_Generic {

    private Connection conn;
    private final String URL_ERROR = "URL is null or length is 0.";
    private final String USERNAME_ERROR = "Username is null or length is 0.";
    private final String PASSWORD_ERROR = "Password is null or length is 0.";
    /**
     * Opens up a basic connection to a database based on driver, url, username
     * and password
     *
     *
     * @param driverClassName - String of the driver class being used
     * @param url - URL to the database
     * @param username - Username for the database
     * @param password - Password for the database
     * @throws IllegalArgumentException
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void openConnection(String driverClassName, String url, String username, String password)
            throws IllegalArgumentException, ClassNotFoundException, SQLException {
        
        if (url == null || url.length() == 0) {
            throw new IllegalArgumentException(URL_ERROR);
        } else if (username == null || username.length() == 0){
            throw new IllegalArgumentException(USERNAME_ERROR);
        } else if (password == null || password.length() == 0){
            throw new IllegalArgumentException(PASSWORD_ERROR);
        } else {
        username = (username == null) ? "" : username;
        password = (password == null) ? "" : password;
        Class.forName(driverClassName);
        conn = DriverManager.getConnection(url, username, password);
    }}

    /**
     * Finds all the records in a table based on the column names. Also allows
     * for sorting and how to sort m
     *
     * @param tableName Requires a String for the table name
     * @param columnNames Requires a List of Strings for the column names
     * @param sortCol Requires a String for the column to use for sorting
     * @param sortAsc Requires a boolean for if want to sort ascending or
     * descending
     * @return Returns a list of maps of the values in the database
     * @throws SQLException Throws a SQL Exception error when SQL Statement
     * incorrect
     */
    public List<Map> findRecords(String tableName, List<String> columnNames, String sortCol, Boolean sortAsc) throws SQLException {
        Statement stmt = null;
        ResultSet rs = null;
        ResultSetMetaData metaData = null;
        final List list = new ArrayList();
        Map record = null;

        //Create String for Sql Query
        String sql = "SELECT ";
        for (String col : columnNames) {
            sql += col + ",";
        }
        sql = sql.substring(0, sql.length() - 1);
        sql += " FROM " + tableName + " ORDER BY " + sortCol;

        //Get results
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            metaData = rs.getMetaData();
            final int fields = metaData.getColumnCount();
            while (rs.next()) {
                record = new HashMap();
                for (int i = 1; i <= fields; i++) {
                    record.put(metaData.getColumnName(i), rs.getObject(i));

                }
                list.add(record);
            }
        } catch (SQLException sqle) {
            throw sqle;
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                stmt.close();
                if (true) {
                    conn.close();
                }
            } catch (SQLException e) {
                throw e;
            }
        }

        return list;
    }
    /**
     * Allows you to find a record by a value using the column name being used
     * 
     * @param tableName Requires a string for the table name
     * @param columnNames Requires a list of strings for the column names
     * @param columnName Requires a String that will be the column searching from
     * @param value Requires an object that will be converted if value is a string, double...etc
     * @return Returns a Map
     * @throws SQLException Throws SQL Exception when statement is invalid.
     */
    public Map findRecordByValue(String tableName, List<String> columnNames, String columnName, Object value) throws SQLException{
        Statement stmt = null;
        ResultSet rs = null;
        ResultSetMetaData metaData = null;
        final List list = new ArrayList();
        Map record = null;
        //SQL Statement being made
        String sql = "SELECT ";
        for (String col : columnNames) {
            sql += col + ",";
        }
        sql = sql.substring(0, sql.length() - 1);
        sql += " FROM " + tableName + " WHERE  " + columnName + " = ";
        
                if (value instanceof String) {
                    sql+= value.toString();
                } else if (value instanceof Integer) {
                    sql+= Integer.parseInt(value.toString());
                } else if (value instanceof Long) {
                    sql+= Long.parseLong(value.toString());
                } else if (value instanceof Double) {
                    sql+= Double.parseDouble(value.toString());
                } else if (value instanceof java.sql.Date) {
                    sql += (java.sql.Date) value;
                } else if (value instanceof Boolean) {
                    sql += (boolean)value;
                } else if (value != null) {
                    sql += value;
                }

       
        //Run statement

        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            metaData = rs.getMetaData();
            final int fields = metaData.getColumnCount();
            while (rs.next()) {
                record = new HashMap();
                for (int i = 1; i <= fields; i++) {
                    record.put(metaData.getColumnName(i), rs.getObject(i));
                }
            }

        } catch (SQLException sqlException) {
            throw sqlException;
        } finally {
            try {
                stmt.close();
                if (Boolean.TRUE) {
                    conn.close();
                }
            } catch (SQLException sqlExcept) {
                throw sqlExcept;
            }
        }

        return record;
    }
    /**
     * Find a record by an ID
     *
     * @param tableName Requires String value of the table name
     * @param columnNames Requires a List of String values for the column names
     * @param idColumnName Requires a String of the ID column name
     * @param item_id Requires an Int for the ID
     * @return Returns a map of values of one record
     * @throws SQLException Throws exception when SQL statement invalid
     */
    
    public Map findRecordById(String tableName, List<String> columnNames, String idColumnName, int item_id) throws SQLException {

        Statement stmt = null;
        ResultSet rs = null;
        ResultSetMetaData metaData = null;
        final List list = new ArrayList();
        Map record = null;
        //SQL Statement being made
        String sql = "SELECT ";
        for (String col : columnNames) {
            sql += col + ",";
        }
        sql = sql.substring(0, sql.length() - 1);
        sql += " FROM " + tableName + " WHERE  " + idColumnName + " = " + item_id;
        //Run statement

        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            metaData = rs.getMetaData();
            final int fields = metaData.getColumnCount();
            while (rs.next()) {
                record = new HashMap();
                for (int i = 1; i <= fields; i++) {
                    record.put(metaData.getColumnName(i), rs.getObject(i));
                }
            }

        } catch (SQLException sqlException) {
            throw sqlException;
        } finally {
            try {
                stmt.close();
                if (Boolean.TRUE) {
                    conn.close();
                }
            } catch (SQLException sqlExcept) {
                throw sqlExcept;
            }
        }

        return record;

    }

    /**
     * Delete a record based on table name, where field to match, where value
     * that can be object and if should close connection or keep connected
     *
     * @param tableName Requires a String for the table name
     * @param whereField Requires a String for the where field that is being
     * checked
     * @param whereValue Requires an object to use for the where value
     * @param closeConnection Requires a boolean whether a connection should
     * close or stay open
     * @return Returns an Int number of records updated.
     * @throws SQLException Throws Exception when SQL Statement is invalid
     */
    public int deleteRecord(String tableName, String whereField, Object whereValue, Boolean closeConnection) throws SQLException {
        PreparedStatement prepState = null;
        int recordsDeleted = 0;

        try {
            prepState = buildRemovalStatement(conn, tableName, whereField);

            if (whereField != null) {
                if (whereValue instanceof String) {
                    prepState.setString(1, ((String) whereValue).toString());
                } else if (whereValue instanceof Integer) {
                    prepState.setInt(1, ((Integer) whereValue).intValue());
                } else if (whereValue instanceof Long) {
                    prepState.setLong(1, ((Long) whereValue).longValue());
                } else if (whereValue instanceof Double) {
                    prepState.setDouble(1, ((Double) whereValue).doubleValue());
                } else if (whereValue instanceof java.sql.Date) {
                    prepState.setDate(1, (java.sql.Date) whereValue);
                } else if (whereValue instanceof Boolean) {
                    prepState.setBoolean(1, ((Boolean) whereValue).booleanValue());
                } else if (whereValue != null) {
                    prepState.setObject(1, whereValue);
                }

            }
            recordsDeleted = prepState.executeUpdate();

        } catch (SQLException sqlException) {
            throw sqlException;
        } finally {
            try {
                prepState.close();
                if (closeConnection) {
                    conn.close();
                }
            } catch (SQLException sqlExcept) {
                throw sqlExcept;
            }
        }

        return recordsDeleted;
    }

    /**
     * Insert a record based on the table name, where field, values, and if the
     * connection should be closed or kept open
     *
     *
     * @param tableName Requires a String value for the table name
     * @param whereField Requires a List of Strings for the where fields that
     * need to be inserted
     * @param whereValues Requires a list of objects that are the values that
     * will be inserted
     * @param closeConnection Requires a boolean that tell the connection to
     * close or stay open
     * @return Returns an int of the number of records updated
     * @throws SQLException Throws an exception when an invalid SQL statement is
     * created.
     */
    public int insertRecord(String tableName, List<String> whereField, List<Object> whereValues, Boolean closeConnection) throws SQLException {
        PreparedStatement prepState = null;
        int recordsInserted = 0;

        try {
            //Build your insert statement   
            prepState = buildInsertStatement(conn, tableName, whereField);

            final Iterator i = whereValues.iterator();
            int index = 1;
            while (i.hasNext()) {
                final Object obj = i.next();
                //Adding to this would allow you to add more than variable types.
                if (obj instanceof String) {
                    prepState.setString(index++, (String) obj);
                } else if (obj instanceof Integer) {
                    prepState.setInt(index++, ((Integer) obj).intValue());
                } else if (obj instanceof Long) {
                    prepState.setLong(index++, ((Long) obj).longValue());
                } else if (obj instanceof Double) {
                    prepState.setDouble(index++, ((Double) obj).doubleValue());
                } else if (obj instanceof java.sql.Date) {
                    prepState.setDate(index++, (java.sql.Date) obj);
                } else if (obj instanceof Boolean) {
                    prepState.setBoolean(index++, ((Boolean) obj).booleanValue());
                } else {
                    if (obj != null) {
                        prepState.setObject(index++, obj);
                    }
                }
            }
         
            recordsInserted = prepState.executeUpdate();

        } catch (SQLException sqlException) {
            throw sqlException;
        } finally {
            try {
                prepState.close();
                if (closeConnection) {
                    conn.close();
                }
            } catch (SQLException sqlExcept) {
                throw sqlExcept;
            }
        }

        return recordsInserted;
    }
/**
 * Allows you to update a record based off of a where string
 * 
 * @param tableName Requires a String for the table name
 * @param columnsBeingUpdated Requires a list of Strings for the columns to be updated
 * @param updateValues Requires a list of objects for the updated values
 * @param whereField Requires a String for there where field that is being looked for
 * @param whereValue Requires an object for the where value that matches what being looked for
 * @param closeConnection Requires a boolean to close connection or leave open
 * @return Returns an int value of how many records were updated
 * @throws SQLException Throws an exception error when SQL Statement is invalid.
 */
    public int updateRecord(String tableName, List<String> columnsBeingUpdated, List<Object> updateValues, String whereField, Object whereValue, Boolean closeConnection) throws SQLException {
        PreparedStatement prepState = null;
        int recordsUpdated = 0;

        try {

            prepState = buildUpdateStatement(conn, tableName, columnsBeingUpdated, whereField, whereValue);

            final Iterator i = updateValues.iterator();
            int index = 1;
            while (i.hasNext()) {
                final Object obj = i.next();
                //Adding to this would allow you to add more than variable types.
                if (obj instanceof String) {
                    prepState.setString(index++, (String) obj);
                } else if (obj instanceof Integer) {
                    prepState.setInt(index++, ((Integer) obj).intValue());
                } else if (obj instanceof Long) {
                    prepState.setLong(index++, ((Long) obj).longValue());
                } else if (obj instanceof Double) {
                    prepState.setDouble(index++, ((Double) obj).doubleValue());
                } else if (obj instanceof java.sql.Date) {
                    prepState.setDate(index++, (java.sql.Date) obj);
                } else if (obj instanceof Boolean) {
                    prepState.setBoolean(index++, ((Boolean) obj).booleanValue());
                } else {
                    if (obj != null) {
                        prepState.setObject(index++, obj);
                    }
                }
            }
            recordsUpdated = prepState.executeUpdate();

        } catch (SQLException sqlException) {
            throw sqlException;
        } finally {
            try {
                prepState.close();
                if (closeConnection) {
                    conn.close();
                }
            } catch (SQLException sqlExcept) {
                throw sqlExcept;
            }
        }

        return recordsUpdated;
    }

    /**
     * Builds a prepared statement for making the removal of a record in a MySQL
     * database
     *
     *
     * @param conn_loc Requires a Connection to be able to pass the final SQL
     * Statement
     * @param tableName Requires a String value of the table name
     * @param whereField Requires a String of the field that will be used to
     * find the record to delete
     * @return Returns a prepared statement which is used to run the SQL
     * statement
     * @throws SQLException Throws an exception error when SQL statement is
     * invalid.
     */
    private PreparedStatement buildRemovalStatement(Connection conn_loc, String tableName, String whereField)
            throws SQLException {
        final StringBuffer sb = new StringBuffer("DELETE FROM ");

        if (tableName != null && whereField != null) {
            sb.append(tableName);
            sb.append(" WHERE ");
            (sb.append(whereField)).append(" = ?");
        }
        final String finalSqlStatement = sb.toString();

        return conn_loc.prepareStatement(finalSqlStatement);

    }

    /**
     * Builds a prepared statement that will be used for inserting a new record
     * into a MySQL database
     *
     *
     *
     * @param conn_loc Requires a Connection object to be able to pass the sql
     * statement
     * @param tableName Requires a String of the table name
     * @param cols Requires a List of Strings for the column names
     * @return Returns a Prepared Statement to be used for a SQL query
     * @throws SQLException Throws exception error when SQL statement is invalid
     */
    private PreparedStatement buildInsertStatement(Connection conn_loc, String tableName, List<String> cols) throws SQLException {
        String finalSqlStatement = null;
        final StringBuffer sb = new StringBuffer("INSERT INTO ");
        String temp = null;

        if (tableName != null) {
            sb.append(tableName);
            sb.append(" (");
            for (String col : cols) {
                sb.append(col).append(" ,");
            }

            temp = sb.substring(0, sb.length() - 1) + ") VALUES (";

            sb.replace(0, sb.length(), temp);
            for (int i = 0; i < cols.size(); i++) {
                temp += " ?,";
            }
            temp = temp.substring(0, temp.length() - 1) + " )";
            sb.replace(0, sb.length(), temp);
        }

        finalSqlStatement = sb.toString();
        return conn_loc.prepareStatement(finalSqlStatement);
    }

    /**
     * Builds a prepared statement to use to make the sql of the update
     * statement
     *
     * @param conn_loc Requires a Connection object
     * @param tableName Requires a string for the table name
     * @param colNames Requires a list of Strings for the columns to be updated
     * @param whereField Requires a string of the where field
     * @return Returns a preparedStatement for use with SQL query
     * @throws SQLException throws exception error when invalid sql statement
     */
    public PreparedStatement buildUpdateStatement(Connection conn_loc, String tableName, List<String> colNames, String whereField, Object whereValue) throws SQLException {
        String finalSqlStatement = null;
        final StringBuffer sb = new StringBuffer("UPDATE ");
        String temp = null;

        if (tableName != null) {
            sb.append(tableName);
            sb.append(" SET ");
            for (String col : colNames) {
                sb.append(col).append("=?,");
            }

            temp = sb.substring(0, sb.length() - 1) + " WHERE ";

            sb.replace(0, sb.length(), temp);
            sb.append(whereField).append("= \'");
            if (whereValue instanceof String) {
                sb.append((String) whereValue);
            } else if (whereValue instanceof Integer) {
                sb.append(((Integer) whereValue).intValue());
            } else if (whereValue instanceof Long) {
                sb.append(((Long) whereValue).longValue());
            } else if (whereValue instanceof Double) {
                sb.append(((Double) whereValue).doubleValue());
            } else if (whereValue instanceof java.sql.Date) {
                sb.append((java.sql.Date) whereValue);
            } else if (whereValue instanceof Boolean) {
                sb.append(((Boolean) whereValue).booleanValue());
            } else {
                if (whereValue != null) {
                    sb.append(whereValue);
                }
            }
            sb.append("\'");
        }

        finalSqlStatement = sb.toString();
        return conn_loc.prepareStatement(finalSqlStatement);
    }

    /**
     * Basic Main for use to test database
     *
     * @param args
     * @throws IllegalArgumentException
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws DataAccessException
     * @throws Exception
     */
    public static void main(String[] args) throws IllegalArgumentException, SQLException, ClassNotFoundException, Exception {
        DB_Generic db = new DB_Generic();
        db.openConnection("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/bookstore", "root", "password");
        List<String> columns = new ArrayList<>();
        columns.add("book_id");
        columns.add("book_name");
        columns.add("book_desc");
//
//        List<Object> updateValues = new ArrayList<>();
//        
//        updateValues.add("Blah");
//        updateValues.add("Testing");
        db.openConnection("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/bookstore", "root", "password");
        //System.out.println(db.updateRecord("books", columns, updateValues, "book_id", 1, Boolean.TRUE));
        db.openConnection("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/bookstore", "root", "password");

        System.out.println(db.findRecords("books", columns, "book_id", Boolean.TRUE));
    }
}
